FROM ubuntu:18.04

### 
# Begin Core NOVNC Container
###
ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root


RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update && apt-get dist-upgrade -y

RUN apt-get install -y --fix-missing --no-install-recommends \
        python-numpy \
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*


### begin noVNC  ###
WORKDIR /opt
RUN apt update -y
RUN apt install build-essential pkg-config libvncserver-dev libssl-dev xorg-dev autoconf -y
RUN wget https://github.com/LibVNC/x11vnc/archive/0.9.16.tar.gz
RUN tar -xvf 0.9.16.tar.gz
WORKDIR /opt/x11vnc-0.9.16
RUN autoreconf -fiv
RUN ./configure
RUN make
RUN make install
### end noVNC ###


RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

RUN echo "this needs to run again, delete this line whenever"
ADD startup.sh /
ADD cleanup-cruft.sh /
ADD initialize.sh /

ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/

# make sure the noVNC self.pem cert file is only readable by root
RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf
### 
# END Core NOVNC Container
###


### 
# Begin Class/Contanier Specific Installs
###
RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow


### version of java requested by 250  ###
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update
RUN apt-get install -y openjdk-8-jdk


### begin visual studio ###
WORKDIR /opt
RUN apt-get install -y libnotify4 libnss3
RUN wget -O visual_studio.deb https://go.microsoft.com/fwlink/\?LinkID\=760868
RUN dpkg -i visual_studio.deb
WORKDIR /


WORKDIR /opt
### spim and qtspim ###
RUN apt-get update
RUN apt-get install -y spim
RUN apt-get install -y libqt4-dev
RUN wget -O qtspim.deb https://sourceforge.net/projects/spimsimulator/files/qtspim_9.1.20_linux64.deb/download
RUN dpkg -i qtspim.deb

### logisim-evolution ###
RUN wget -O logisim-generic-2.7.1.jar https://sourceforge.net/projects/circuit/files/2.7.x/2.7.1/logisim-generic-2.7.1.jar/download
RUN wget -O logisimevolution.jar https://sourceforge.net/projects/logisimevolution/files/latest/download
ADD run-logisim /bin
ADD logisim.png /opt
ADD logisim.desktop /usr/share/applications
# ADD logisim_cli.jar /bin

### terminal editors ###
RUN apt-get install -y vim
RUN apt-get install -y nano
RUN apt-get install -y emacs

### valgrind ###
RUN apt-get update -y 
RUN apt-get install -y valgrind

### venus ###
ADD venus.jar /opt
ADD venus /bin
### 
# END Class/Contanier Specific Installs
###




# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6

ENTRYPOINT ["/startup.sh"]